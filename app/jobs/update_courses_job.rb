require 'json'

class UpdateCoursesJob < ApplicationJob
  queue_as :default

  ROWS_PER_CALL = 24
  PAUSE_INTERVAL = 5.seconds

  def self.getUrl(start, rows)
    "https://www.myskillsfuture.sg/services/tex/individual/course-search" \
    "?query=rows%3D#{rows}" \
    "%26fq%3DIsValid%253Atrue" \
    "%26q%3D*%253A*" \
    "%26start%3D#{start * rows}"
  end 

  def perform(*args)
    Course.transaction do
      Course.delete_all

      i = 0
      loop do 
        courses, totalCourses = getCourses(i)
        
        UpdateCoursesJob.printStats(totalCourses) if i == 0
        break if courses.length == 0 # reach end of courses
        
        Course.import! courses, on_duplicate_key_ignore: true
        
        puts "Iteration #{i + 1} completed. Current courses so far: #{Course.count}. Pausing before next iteration..."
        sleep(PAUSE_INTERVAL)

        i += 1
      end
    end

    puts 'Job completed!'
  end

  def getCourses(start)
      url = UpdateCoursesJob.getUrl(start, ROWS_PER_CALL)
      puts "Calling URL: #{url}"

      response = HTTParty.get(url)
      jsonBody = JSON.parse(response.body)
      
      totalCourses = jsonBody['grouped']['GroupID']['matches']
      rawCourses = jsonBody['grouped']['GroupID']['groups']

      courses = rawCourses.map do |c|
        rawCourse = c['doclist']['docs'].first
        UpdateCoursesJob.mapToCourse(rawCourse)
      end

      [courses, totalCourses]
  end

  def self.mapToCourse(rawCourse)
    {
      refNo: rawCourse['Course_Ref_No'],
      title: rawCourse['Course_Title'],
      objective: rawCourse['Course_Objective'],
      content: rawCourse['Course_Content'],
      organisation: rawCourse['Organisation_Name'],
      courseCreatedDate: rawCourse['Course_Created_Date'],
      featured: rawCourse['Featured'],
      popular: rawCourse['Popular'],
      costPerTrainee: rawCourse['Tol_Cost_of_Trn_Per_Trainee'],
      durationHours: rawCourse['Total_Training_Duration_Hrs'],
      durationDays: rawCourse['Len_of_Course_Duration'],
      areasOfTraining: rawCourse['Area_of_Training'] || [],
      modesOfTraining: rawCourse['Mode_of_Training'] || [],
      languages: rawCourse['Medium_of_Instruction'] || [],
      claimStartDate: rawCourse['Course_Supp_Period_Frm_1'],
      claimEndDate: rawCourse['Course_Supp_Period_To_1'],
      minimumQualitifcations: rawCourse['Minimum_Education_Req'],
      image: rawCourse['Display_Image_Name'],
      hasCourseRun: rawCourse['HasCourseRun'],
      courseRunCount: rawCourse['CourseRunCount'],
      mostViewed: rawCourse['MostViewed'],
      version: rawCourse['_version_'],
      vacancies: rawCourse['Course_Vacancy'] || []
    }
  end

  def self.printStats(totalCourses)
    iterationsRequired = (totalCourses / ROWS_PER_CALL).ceil
    estimatedTimeRequired = iterationsRequired * (0.3.seconds + PAUSE_INTERVAL)
    puts "Total courses to pull: #{totalCourses}."
    puts "Iterations required: #{iterationsRequired}."
    puts "Estimated time required: #{Time.at(estimatedTimeRequired).utc.strftime("%H:%M:%S")}."
    puts '=================================='
  end
end
