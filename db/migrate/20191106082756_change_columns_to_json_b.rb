class ChangeColumnsToJsonB < ActiveRecord::Migration[5.2]
  def up
    remove_column :courses, :areaOfTraining
    remove_column :courses, :modeOfTraining
    remove_column :courses, :language
    remove_column :courses, :vacancy

    add_column :courses, :areasOfTraining, :text, default: [], array: true
    add_column :courses, :modesOfTraining, :text, default: [], array: true
    add_column :courses, :languages, :text, default: [], array: true
    add_column :courses, :vacancies, :text, default: [], array: true
  end

  def down
    remove_column :courses, :areasOfTraining
    remove_column :courses, :modesOfTraining
    remove_column :courses, :languages
    remove_column :courses, :vacancies

    add_column :courses, :areaOfTraining, :string
    add_column :courses, :modeOfTraining, :string
    add_column :courses, :language, :string
    add_column :courses, :vacancy, :string
  end
end
