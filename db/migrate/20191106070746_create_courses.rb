class CreateCourses < ActiveRecord::Migration[5.2]
  def change
    create_table :courses do |t|
      t.string :refNo
      t.string :title
      t.text :objective
      t.text :content
      t.string :organisation
      t.date :courseCreatedDate
      t.boolean :featured
      t.boolean :popular
      t.decimal :costPerTrainee
      t.integer :durationHours
      t.integer :durationDays
      t.string :areaOfTraining
      t.string :modeOfTraining
      t.string :language
      t.date :claimStartDate
      t.date :claimEndDate
      t.string :minimumQualitifcations
      t.string :image
      t.boolean :hasCourseRun
      t.integer :courseRunCount
      t.integer :mostViewed
      t.string :version
      t.integer :viewCount
      t.string :vacancy

      t.timestamps
    end
    add_index :courses, :refNo, unique: true
  end
end
