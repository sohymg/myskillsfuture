# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_11_06_082756) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "courses", force: :cascade do |t|
    t.string "refNo"
    t.string "title"
    t.text "objective"
    t.text "content"
    t.string "organisation"
    t.date "courseCreatedDate"
    t.boolean "featured"
    t.boolean "popular"
    t.decimal "costPerTrainee"
    t.integer "durationHours"
    t.integer "durationDays"
    t.date "claimStartDate"
    t.date "claimEndDate"
    t.string "minimumQualitifcations"
    t.string "image"
    t.boolean "hasCourseRun"
    t.integer "courseRunCount"
    t.integer "mostViewed"
    t.string "version"
    t.integer "viewCount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "areasOfTraining", default: [], array: true
    t.text "modesOfTraining", default: [], array: true
    t.text "languages", default: [], array: true
    t.text "vacancies", default: [], array: true
    t.index ["refNo"], name: "index_courses_on_refNo", unique: true
  end

end
